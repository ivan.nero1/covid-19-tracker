import React from 'react';
import classes from './App.module.css'
import { fetchData } from './api/index'

import { InfoCards, Chart, CountryPicker, Logo } from './components'

class App extends React.Component {

  state = {
    data: {},
    country: ''
  }

  async componentDidMount() {
    const fetchedData = await fetchData()

    this.setState({ data: fetchedData })

  }

  handleCountryChange = async (country) => {
    const fetchedData = await fetchData(country)
    this.setState({ data: fetchedData, country: country })
    console.log(fetchedData)
  }

  render() {
    const { data, country } = this.state
    return(
      <div className={classes.container}>
        <Logo /> 
        <InfoCards data={data} />
        <CountryPicker handleCountryChange={this.handleCountryChange} />
        <Chart data={data} country={country} />
      </div>
    )
  }
}

export default App;
