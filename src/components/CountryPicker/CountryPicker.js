import React, {useState, useEffect} from 'react';
import classes from './CountryPicker.module.css'
import { NativeSelect, FormControl } from '@material-ui/core';
import { fetchCountries } from '../../api'

const CountryPicker = (props) => {

    const {handleCountryChange} = props

    const [countries, setCountries] = useState([])    
    useEffect(() => {
        const fetchAPI = async () => {
            setCountries(await fetchCountries())
        }

        fetchAPI()
    },[setCountries]) 

    return (
        <div className={classes.CountryPicker}>
            <FormControl>
                <NativeSelect defaultValue="" onChange={(e) => handleCountryChange(e.target.value)}>
                    <option value=''>Global</option>
                    {countries.map((country, i) => {
                        return <option value={country} key={i}>{country}</option>
                    }) }
                    
                </NativeSelect>
            </FormControl>
        </div>
    );
}

export default CountryPicker;
