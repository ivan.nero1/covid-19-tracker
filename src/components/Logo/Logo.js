import React from 'react'
import classes from './Logo.module.css'

export default function Logo() {
    return (
        <h1 className={classes.Logo}>
            COVID-19 TRACKER
        </h1>
    )
}
