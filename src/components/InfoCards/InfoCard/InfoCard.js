import React from 'react'
import classes from './InfoCard.module.css'

import { Card, CardContent, Typography, Grid } from '@material-ui/core'
import CountUp from 'react-countup'

import cx from 'classnames'

const InfoCard = ({ title, displayData, lastUpdate }) => {
    return (
        <Grid item component={Card} xs={12} md={3}  className={cx(classes.Card, classes[title] )} >
        <CardContent>
            <Typography color="textSecondary" gutterBottom>{title}</Typography>
            <Typography variant="h5" >
                <CountUp start={0} end={displayData} separator=" " />
            </Typography>
            <Typography color="textSecondary" > { new Date(lastUpdate).toDateString() } </Typography>
            <Typography variant="body2" >Number of {title.toLowerCase()} cases of COVID-19 </Typography>
        </CardContent>
    </Grid>
    )
}

export default InfoCard
