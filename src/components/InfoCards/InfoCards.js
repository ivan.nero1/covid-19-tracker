import React from 'react';
import classes from './InfoCards.module.css'
import InfoCard from './InfoCard/InfoCard'
import { Grid } from '@material-ui/core'
import Loader from '../Loader/Loader'
 

const InfoCards = (props) => {
    const { data: { confirmed, recovered, deaths, lastUpdate } } = props
    if(!confirmed) {
        return <Loader />
    }

    return (
        <div className={classes.container}>
            <Grid container spacing={3} justify='center'>
                <InfoCard displayData={confirmed.value} lastUpdate={lastUpdate} title="Infected"  />
                <InfoCard displayData={recovered.value} lastUpdate={lastUpdate} title="Recovered" />
                <InfoCard displayData={deaths.value} lastUpdate={lastUpdate} title="Deaths" />
            </Grid>
        </div>
    );
}

export default InfoCards;
