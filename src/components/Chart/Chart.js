import React, { useState, useEffect } from 'react';
import { fetchDailyData } from '../../api/index'
import { Line, Bar } from 'react-chartjs-2'
import Loader from '../Loader/Loader'

import classes from './Chart.module.css'

const Chart = ({data: {confirmed, recovered, deaths}, country}) => {

    const [dailyData, setDailyData] = useState([])

    useEffect(() => {
        const fetchAPI = async () => {
            setDailyData(await fetchDailyData())
        }

        fetchAPI()
    },[] ) 

    const lineChart = (
        dailyData.length
        ? <Line
            data={{
                labels: dailyData.map(({date}) => date) ,
                datasets: [{
                    data: dailyData.map(({confirmed}) => confirmed),
                    label: "Infected",
                    borderColor: '#3333ff',
                    fill: true
                        },
                    {
                        data: dailyData.map(({deaths}) => deaths),
                        label: "Deaths",
                        borderColor: 'rgba(255,0,0,.5)',
                        fill:true
                    }
                ],

            }} /> : <Loader />
    )

    const barCahart = (
        confirmed
        ? <Bar 
            data={{
                labels: ['Infected', 'Recovered',  'Deaths', 'Active', ],
                datasets: [{
                    label: 'People',
                    backgroundColor: [
                        'rgba(0,0, 225, .5)', 
                        'rgba(0,255, 0, .5)',
                        'rgba(255,0, 0, .5)',
                        'rgba(255,0, 225, .5)',
                        ],
                    data: [confirmed.value, recovered.value,  deaths.value, (confirmed.value - (recovered.value + deaths.value)),]
                }]
            }}
            options={{
                legend: {display: false},
                title: { display:true, text: `Currient state in ${country}` }
            }} /> : <Loader />
    )

    return (
        <div className={classes.Chart}>
            { country ? barCahart : lineChart}
            
        </div>
    );
}

export default Chart;
